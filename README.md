<img src="https://pub-00bc7a7c079d493391109fe14bbac1f3.r2.dev/destruct%20-%20icons.webp" alt="abstract - icons" width="100%">

# DESTRUCT ICONS
A Blender project for generating procedural icons used by @Destruct repositories
The project file is [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

Requires [Blender 4.0.2](https://www.blender.org/)
